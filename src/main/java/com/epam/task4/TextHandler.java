package com.epam.task4;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TextHandler {
    private static final Logger logger = LogManager.getLogger(TextHandler.class);
    private static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

    private List<String> text;

    public TextHandler() {
        text = new ArrayList<>();
    }

    public void setText(List<String> text) {
        this.text = text;
        logger.info("text for updated " + text);
    }

    public List<String> getSortedUniqueWords(){
        List<String> sortedUniqueWords = text.stream()
                .flatMap((String line) -> Arrays.stream(line.trim().split("\\W+")))
                .distinct()
                .sorted()
                .collect(Collectors.toList());
        logger.info("returned list of sorted unique word from text " + sortedUniqueWords);
        return sortedUniqueWords;
    }

    public long getNumOfUniqueWords(){
        long numbOfUniqueWords = text.stream()
                .flatMap(line -> Arrays.stream(line.trim().split("\\W+")))
                .distinct()
                .count();
        logger.info("returned number of unique words from text " + numbOfUniqueWords);
        return numbOfUniqueWords;
    }

    public Map<String, Long> getWordCount(){
        Map<String, Long> wordCount = text.stream()
                .flatMap(line -> Arrays.stream(line.trim().split("\\W+")))
                .collect(Collectors.groupingBy(word -> word, Collectors.counting()));
        logger.info("returned words count " + wordCount);
        return wordCount;
    }

    public long getNumbOfLowerCaseSymbol(){
        long numbOfLowerCaseSymbols = text.stream()
                .flatMap(line -> Arrays.stream(line.trim().split("\\W+")))
                .count();
        logger.info("returned number of lower case symbol in text " + numbOfLowerCaseSymbols);
        return numbOfLowerCaseSymbols;
    }

    public static void main(String[] args) {
        TextHandler textHandler = new TextHandler();
        textHandler.getNumbOfLowerCaseSymbol();
    }
}
