package com.epam.task3;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class RandomArrayList {
    private static Random random = new Random();
    static final int MAX_NUMBER = 1_000;
    static final int SIZE_LIST = 10;

    public List<Integer> createList(int sizeList) {
        List<Integer> integers = new ArrayList<>();
        for (int i = 0; i < sizeList; i++) {
            integers.add(random.nextInt(MAX_NUMBER));
        }
        return integers;
    }

    public List<Integer> createLambdaList(int sizeList) {
        List<Integer> integers = random
                .ints(0, MAX_NUMBER)
                .limit(sizeList)
                .boxed()
                .collect(Collectors.toList());
        return integers;
    }

    public List<Integer> createStreamList(int sizeList) {
        List<Integer> integers = ThreadLocalRandom
                .current()
                .ints(sizeList, 0, MAX_NUMBER)
                .boxed()
                .collect(Collectors.toList());
        return integers;
    }

    public int[] createStreamArray(int sizeArray) {
        int[] ints = IntStream.generate(() -> random.nextInt(MAX_NUMBER))
                .limit(sizeArray)
                .toArray();
        return ints;
    }

    public int[] createLambdaArray(int sizeArray) {
        int[] ints = random
                .ints(sizeArray, 0, MAX_NUMBER)
                .toArray();
        return ints;
    }

//    public void countAverageMinMaxSumOfList(List<Integer> list)




}
