package com.epam.task3;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.List;

public class MathServices {
    private List<Integer> list;
    private int[] array;
    private static RandomArrayList randomArrayList;
    private static Logger logger = LogManager.getLogger(MathServices.class);

    public MathServices() {
        randomArrayList = new RandomArrayList();
        list = randomArrayList.createList(10);
        array = randomArrayList.createLambdaArray(10);
    }

    public List<Integer> getList() {
        return list;
    }

    public int getMaxValueOfList() {
        return list.stream()
                .mapToInt(Integer::intValue)
                .max()
                .getAsInt();
    }

    public int getMinValueOfList() {
        return list.stream()
                .mapToInt(Integer::intValue)
                .min()
                .getAsInt();
    }

    public double getAverageValueOfList() {
        return list.stream()
                .mapToInt(Integer::intValue)
                .average()
                .getAsDouble();
    }

    public int getSumValueOfList() {
        return list.stream()
                .reduce(0, Integer::sum);
    }

    public long getNumberBiggerThanAverage() {
        return list.stream()
                .filter(number -> number > getAverageValueOfList())
                .distinct()
                .count();
    }

    public int getMaxValueOfArray() {
        return Arrays.stream(array)
                .max()
                .getAsInt();

    }

    public int getMinValueOfArray() {
        return Arrays.stream(array)
                .min()
                .getAsInt();
    }

    public double getAverageValueOfArray() {
        return Arrays.stream(array)
                .average()
                .orElse(Double.NaN);
    }

    public void showList(/*List<Integer> list*/) {
        list.forEach(element -> System.out.print(element + "\t"));
        System.out.println();
    }

    public void showArray(int[] array) {
        Arrays.stream(array)
                .forEach(element -> System.out.print(element + "\t"));
        System.out.println();
    }

    public static void main(String[] args) {
        MathServices mathServices = new MathServices();
        logger.info("print list " + mathServices.list.toString());
        logger.info("mix value = " + mathServices.getMinValueOfList());
        logger.info("max value = " + mathServices.getMaxValueOfList());
        logger.info("average value = " + mathServices.getAverageValueOfList());
        logger.info("sum numbers of list = " + mathServices.getSumValueOfList());
        logger.info("" + mathServices.getNumberBiggerThanAverage());
        logger.info("\n==============================================");
        logger.info("print array " + Arrays.toString(mathServices.array));
        logger.info("max value = " + mathServices.getMaxValueOfArray());
        logger.info("min value = " + mathServices.getMinValueOfArray());
        logger.info("average value = " + mathServices.getAverageValueOfArray());

    }
}
