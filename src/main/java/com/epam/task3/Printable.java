package com.epam.task3;

@FunctionalInterface
public interface Printable {

    void print(String line);

}