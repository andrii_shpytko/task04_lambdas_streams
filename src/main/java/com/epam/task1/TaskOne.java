package com.epam.task1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TaskOne {
    private static Logger logger = LogManager.getLogger(TaskOne.class);
    private static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) {
        Calculable getAverage = (num1, num2, num3) -> (num1 + num2 + num3) / 3;
        logger.info("Avg of numbers = " + getAverage.calculate(10, 20, 30));
        Calculable getMax = (num1, num2, num3) -> (Math.max(num1, Math.max(num2, num3)));
        logger.info("Max number = " + getMax.calculate(10, 20, 30));
        Calculable getSum = (num1, num2, num3) -> (num1 + num2 + num3);
        logger.info("Sum of numbers = " + getSum.calculate(40, 30, 20));
        Calculable getSumFromConsole = (num1, num2, num3) -> (num1 + num2 + num3);
        logger.info("Sum of numbers = " +
                getSumFromConsole.calculate(getNumber(br), getNumber(br), getNumber(br)));
    }

    private static int getNumber(BufferedReader br) {
        int numb = 0;
        boolean continueLoop = true;
        do {
            try {
                System.out.print("Input number: ");
                numb = Integer.parseInt(br.readLine());
                continueLoop = false;
            } catch (IOException | NumberFormatException e) {
//                e.printStackTrace();
            }
        } while (continueLoop);
        return numb;
    }

}
