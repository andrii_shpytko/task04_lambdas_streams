package com.epam.task1;

@FunctionalInterface
interface Calculable {
    int calculate(int num1, int num2, int num3);
}
