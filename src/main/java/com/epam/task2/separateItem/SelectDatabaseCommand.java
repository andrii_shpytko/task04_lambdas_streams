package com.epam.task2.separateItem;

import com.epam.task2.DatabaseCommand;
import com.epam.task2.receiver.Database;

public class SelectDatabaseCommand implements DatabaseCommand {
    private Database database;

    public SelectDatabaseCommand(Database database) {
        this.database = database;
    }

    @Override
    public void execute() {
        database.select();
    }
}
