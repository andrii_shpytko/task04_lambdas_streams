package com.epam.task2.separateItem;

import com.epam.task2.DatabaseCommand;
import com.epam.task2.receiver.Database;

public class UpdateDatabaseCommand implements DatabaseCommand {
    private Database database;

    public UpdateDatabaseCommand(Database database) {
        this.database = database;
    }

    @Override
    public void execute() {
        database.update();
    }
}
