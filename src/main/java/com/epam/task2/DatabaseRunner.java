package com.epam.task2;

import com.epam.task2.invoker.Developer;
import com.epam.task2.receiver.Database;
import com.epam.task2.separateItem.DeleteDatabaseCommand;
import com.epam.task2.separateItem.InsertDatabaseCommand;
import com.epam.task2.separateItem.SelectDatabaseCommand;
import com.epam.task2.separateItem.UpdateDatabaseCommand;

public class DatabaseRunner {
    public static void main(String[] args) {
        Database database = new Database();
        Developer developer = new Developer(
                new InsertDatabaseCommand(database),
                new UpdateDatabaseCommand(database),
                new SelectDatabaseCommand(database),
                new DeleteDatabaseCommand(database)
        );

        developer.insertRecord();
        developer.updateRecord();
        developer.selectRecord();
        developer.deleteRecord();
    }
}
