package com.epam.task2.invoker;

import com.epam.task2.DatabaseCommand;

public class Developer {
    DatabaseCommand insert;
    DatabaseCommand select;
    DatabaseCommand delete;
    DatabaseCommand update;

    public Developer(DatabaseCommand insert, DatabaseCommand select, DatabaseCommand delete, DatabaseCommand update) {
        this.insert = insert;
        this.select = select;
        this.delete = delete;
        this.update = update;
    }

    public void insertRecord() {
        insert.execute();
    }

    public void updateRecord() {
        update.execute();
    }

    public void deleteRecord() {
        delete.execute();
    }

    public void selectRecord() {
        select.execute();
    }
}
