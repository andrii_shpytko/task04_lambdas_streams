package com.epam.task2;

@FunctionalInterface
public interface DatabaseCommand {
    void execute();
}
